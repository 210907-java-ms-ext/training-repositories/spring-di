package dev.rehm;

import dev.rehm.models.Breed;
import dev.rehm.models.Dog;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Driver {

    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
        Dog d = (Dog) ac.getBean("dog");
        System.out.println(d);

        System.out.println("Dog named: "+d.getName()+" is of breed: "+d.getBreed().getName());

        Breed b = (Breed) ac.getBean("breed");
        System.out.println(b);
    }

}
